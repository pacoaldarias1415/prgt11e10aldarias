package ejercicio1;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.io.IOException;

/**
 * Version con funciones
 *
 * @author pacoaldarias <paco.aldarias@ceedcv.es>
 */
public class Main {

  public static void borrarBD(String nombrefichero)
          throws IOException {
    System.out.println("* Borrando BD " + nombrefichero);
    File f = new File(nombrefichero);

    Borrarfichero
    
  }

  public static ObjectContainer crearBD(String nombrefichero) {
    System.out.println("* Creando BD " + nombrefichero);
    return Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),
            nombrefichero);
  }

  public static void almacena(ObjectContainer bd) {
    System.out.println("* Grabando Alumnos:");
    alumno a1 = new alumno("Juan", 15, 12.5);

    Guardar a1
    
    System.out.println(a1.getNombre() + " Almacenado");
    alumno a2 = new alumno("Vicente", 50, 20.1);

    Guardar a2
    
    System.out.println(a2.getNombre() + " Almacenado");

  }

  public static void mostrartodos(ObjectContainer bd) {
    alumno a;
    ObjectSet res = null;

    System.out.println("* Mostrando Todos");
    a = new alumno(null, 0, 0);

    Realizar Query sobre a
            
    System.out.println("Objetos alumno recuperados: " + res.size());
    while (res.hasNext()) {
      System.out.println(res.next()); //toString

    }

  }

  public static void mostrarconedad(ObjectContainer bd, int edad) {

    alumno a;
    ObjectSet res = null;

    System.out.println("* Recuperado objetos alumnos de edad " + edad);


    Crear plantilla de busqueda sobre a
    
    
    res = bd.queryByExample(a);
    System.out.println("Objetos alumno recuperados: " + res.size());
    while (res.hasNext()) {
      System.out.println(res.next()); //toString
    }

  }

  public static void modificarnombre(ObjectContainer bd, String nombre) {

    alumno a, b;
    ObjectSet res = null;

    System.out.println("* Modificando " + nombre);
    a = new alumno(nombre, 0, 0);
    res = bd.queryByExample(a);
    b = (alumno) res.next();
    b.setNombre("Oscar");

    Guardar b
    
  }

  public static void borrarnombre(ObjectContainer bd, String nombre) {

    alumno a, b;
    ObjectSet res = null;

    System.out.println("Borrando " + nombre);
    a = new alumno(nombre, 0, 0);
    res = bd.queryByExample(a);
    b = (alumno) res.next();

    Borrar b
    
  }

  public static void main(String args[]) throws IOException {

    ObjectContainer bd = null;
    String nombrefichero = "alumnos.db4o";

    try {
      borrarBD(nombrefichero);
      bd = crearBD(nombrefichero);
      almacena(bd);
      mostrartodos(bd);
      mostrarconedad(bd, 50);
      modificarnombre(bd, "Vicente");
      borrarnombre(bd, "Juan");
    } finally {
      boolean closed = bd.close();
    }
  }
}
