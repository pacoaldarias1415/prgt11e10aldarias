
package ejemplo6;

public class alumno {

  private String nombre;
  private int edad;
  private double nota;

  alumno(String nom, int e, double n) {

    nombre = nom;
    edad = e;
    nota = n;
  }
  
  public void setNombre(String n){
    nombre = n;
  }

  @Override
  public String toString() {
    return "    - Nombre Alumno:"+this.nombre + " Edad:" + this.edad + " Nota:" + this.nota;
  }

  String getNombre() {
    return nombre;
  }
}
